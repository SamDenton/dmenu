**This build of dmenu of part of SÅMDE, it's a arch linux post installation script that recreates my desktop setup. I highly recommend checking it out : [SÅMDE](https://gitlab.com/SamDenton/samde)**

# SÅM's build of dmenu
[Dmenu](https://tools.suckless.org/dmenu) is the dynamic menu made by suckless. I've applied a few patches to implement the features that I want.

## Patches

- barpadding
- borderoption
- caseinsensitive
- center
- fuzzymatch
- lineheight
- mousesupport
- numbers
- xresources-alt

## Installation
```
git clone --depth 1 https://gitlab.com/SamDenton/dmenu.git
cd dmenu
sudo make install
```
